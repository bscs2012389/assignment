include 'emu8086.inc'
.stack 100h
.code
main proc
    
    mov al,8
    mov bl,4
    div bl
    mov bx,ax
     
    print 'R: '
    
    mov dl,bh
    add dl,48
    mov ah,02h
    int 21h
    
    mov dl,10
    mov ah,02h
    int 21h 
    
    mov dl,13
    mov ah,02h
    int 21h
    
    print 'Q: '
    
    mov dl,bl
    add dl,48
    mov ah,02h      
    int 21h
    
    
    main endp
end main 